package com.java1234.mappers;

import com.java1234.model.Student;

public interface StudentMapper {
  
	public Student findStudentWithAddress(Integer id);
	
	public Student findByGradeId(Integer gradeId);
}
