package com.java1234.service;

import org.apache.ibatis.session.SqlSession;

import com.java1234.mappers.AddressMapper;
import com.java1234.model.Address;
import com.java1234.util.SqlSessionFactoryUtil;

public class AddressTest {
	public static void main(String[] args) {
		SqlSession sqlSession=SqlSessionFactoryUtil.openSession();
		AddressMapper  addressMapper=sqlSession.getMapper(AddressMapper.class);
		Address address=addressMapper.findById(2);
		System.out.println(address);
	}
}
