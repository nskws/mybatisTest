package com.java1234.service;

import java.util.Random;

import org.apache.ibatis.session.SqlSession;

import com.java1234.mappers.StudentMapper;
import com.java1234.model.Student;
import com.java1234.util.SqlSessionFactoryUtil;

public class StudentTest {
	public static void main(String[] args) {
		SqlSession sqlSession=SqlSessionFactoryUtil.openSession();
		StudentMapper  studentMapper=sqlSession.getMapper(StudentMapper.class);
		Student student=studentMapper.findStudentWithAddress(3);
		System.out.println(student);
	}
}
