package com.java1234.service;

import org.apache.ibatis.session.SqlSession;

import com.java1234.mappers.GradeMapper;
import com.java1234.model.Grade;
import com.java1234.model.Student;
import com.java1234.util.SqlSessionFactoryUtil;

public class GradeTest {
	public static void main(String[] args) {
		SqlSession sqlSession=SqlSessionFactoryUtil.openSession();
		GradeMapper  gradeMapper=sqlSession.getMapper(GradeMapper.class);
		Grade grade=gradeMapper.findById(3);
		for(Student student:grade.getStudents()){
			System.out.println(student);
		}
	}
}
